README for the Docs for CAFE project

Matt Williams and Tony Hunter (2012)

All software released under GPL v.3: http://www.gnu.org/copyleft/gpl.html
All documentation released under Creative Commons Attribution-NoDerivatives 4.0 International License, except for those elements where copyright lies with external publishers.

Matt Williams: http://www.imperial.ac.uk/people/matthew.williams matthew.williams2-at-imperial.nhs.uk	
Tony Hunter: http://www0.cs.ucl.ac.uk/staff/a.hunter/ anthony.hunter-at-ucl.ac.uk

Further contact details are available through our webpages

See main Readme for an overview

We have provided the tutorial overview and the pre-publication version of the Lung Cancer paper here (se below for reference)

The 2014 figures relate to the Lung Cancer paper

The 2015 figure uses the same dataset, but with the addition of data from the Santana-Davila paper (see below)




References:

Williams M, Liu ZW, Hunter A, Macbeth F.
An updated systematic review of lung chemo-radiotherapy using a new evidence aggregation method.
Lung Cancer. 2015 Mar;87(3):290-5. doi: 10.1016/j.lungcan.2014.12.004. Epub 2014 Dec 20.

Santana-Davila, Devisetty K, Szabo A, Sparapani R, Arce-Lara C, Gore EM, Moran A, Williams CD, Kelley MJ, Whittle J.
Cisplatin and Etoposide Versus Carboplatin and Paclitaxel With Concurrent Radiotherapy for Stage III Non-Small-Cell Lung Cancer: An Analysis of Veterans Health Administration Data.
J Clin Oncol. 2015 Feb 20;33(6):567-74. doi: 10.1200/JCO.2014.56.2587. Epub 2014 Nov 24.
