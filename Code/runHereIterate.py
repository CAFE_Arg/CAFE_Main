#################################################
#################################################
####### PyMed, Mon 15th October 2012
####### Released under the GPL version 3
####### Matt Williams & Tony Hunter
#################################################
import generateTable
import expandTable
import queryTable
import normalizeTable
import generateSuperGraph
import outputFunctions
import compareSuperGraph
import os
import subprocess
import utilityFunctions
import logging as log
import csv
import pdb
log.basicConfig(filename = 'OutputLog.log', level=log.DEBUG)
#################################################
#This is the relatively stable version of the iterated output
#################################################
#Set options here:
prefList = ["prefcriterion1", "prefcriterion2"]
#Remember: Pref2 = Efficacy, Pref1 = Balanced
metarulesList = ["nonStatSig","Grade_NotGood","None"]
#metarulesList = ["None"]
#metarulesList = ["StageII"]
#filename ='LungCRT_3.64.csv'
#filename = 'GBM_RRT1.2.csv'

filename = 'GBM_RRT1.2_GroupB.csv'

#filename = 'GBM_RRT1.4_MGMTBoth.csv'
#filename = 'GBM_RRT1.4_MGMTBoth_ChemoYNRelax.csv'
#filename = 'GBM_RRT1.4_MGMTBoth_GroupA.csv'
#filename = 'GBM_RRT1.5_MGMTBoth_0.3_GroupC.csv'
#filename = 'GBM_RRT1.4_MGMTBoth_GroupC.csv'

inputdirectory = '../Input/'
inputFileName = inputdirectory + filename
#outputdirectory = "../Output"
outputdirectory = "/home/mw/Software/PyMed/Output"
print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "\n"
print "PYMED - Monday 15th March 2012"
print "Read following spreadsheet", inputFileName
log.debug("\n PYMED - Monday 15th March 2012 \n")
evdb = generateTable.getmydb(inputFileName)
evdb = expandTable.addriskratio(evdb)
treatments = queryTable.getmyoplist(evdb)
print "Start superiority graph construction"
treatmentpairs = queryTable.getmyoppairs(evdb)
#print "Call generateSuperGraph.filterpairs"
selectedpairs = generateSuperGraph.filterpairs(treatmentpairs,evdb)
print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "Now iterating over options"
print "Metarules ", metarulesList
print "Prefs ", prefList
log.debug("Metarules: " + str(metarulesList))
log.debug("Prefs: " + str(prefList))
outputResults =[]
outputDotFiles =[]
for rule in metarulesList:
    for pref in prefList:
        print rule, pref
        superResults = generateSuperGraph.makeSuperGraph(selectedpairs,evdb,pref,rule)
        log.debug("superResults Done")
        OutputName = filename + "_" + str(pref) + "_" + str(rule)
        outputFile = "output.html"
        dotOutputName = OutputName + '.dot'
        print "This superiority graph was constructed using pref " + pref + " and Meta-rule " + str(rule) + "\n"
        dir1 = os.path.dirname(os.getcwd())
        outputDirectory = "Output"
        dir = os.path.join(dir1, outputDirectory)
        #These make the top-level files: A normalised evidence table and a picture
        newOutDir = os.path.join(dir, OutputName)
        fullDotOutputName = os.path.join(newOutDir, dotOutputName)
        utilityFunctions.ensure_dir(newOutDir)
        outputFunctions.generateDotFile(inputFileName, dotOutputName, pref, rule, superResults, newOutDir)
        outputFunctions.generateNormalisedTables(evdb, outputFile, fullDotOutputName)
        log.debug("By this point should have outputs and Dot file")
        subprocess.call(['neato', '-Tsvg', '-O', fullDotOutputName], shell = False)
        #subprocess.call(['C:/Program Files (x86)/Graphviz2.38/bin/neato', '-Tsvg', '-O', fullDotOutputName], shell = False)
        imgOutputName = fullDotOutputName + '.svg'
        print "Using dir: ", dir, " as a base."
        print "FDON: ", os.path.abspath(fullDotOutputName)
        print "IMG: ", imgOutputName
        ###We've made a sub-directory, so now fill it with subtables
        outputFunctions.generateSubtables(evdb, selectedpairs, superResults, newOutDir, rule, pref)
        outputResults.append(superResults)
        outputDotFiles.append(os.path.abspath(fullDotOutputName))
        #This then opens files
        os.startfile(imgOutputName)
        os.startfile(outputFile)
#
# d1 = compareSuperGraph.makeDictofPairs(selectedpairs)
# for i in outputResults:
#     #print 'OP: ', i
#     d1 = compareSuperGraph.addSuperGraph(d1,i)
# #for i in d1:
# #    print 'd1: ',i, d1[i]
#
# baseMeta = set(["nonStatSig"])
# basePref = 'prefcriterion1'
# summaryMode = 'simple'
#
# #print 'D1: ', d1
# d4 = compareSuperGraph.markSuperGraphs(d1, baseMeta, basePref)
#
# #d5 = compareSuperGraph.generateMarkedTable(d4, summaryMode)
#
# def outputMarkedTable(table):
#     outfilename = '../Output/Compare/outComp1.csv'
#     secondKey = table.keys()[1]
#     secondDict = table[secondKey][1]
#     header = sorted(secondDict.keys())
#     header.insert(0, 'Regimen2')
#     header.insert(0, 'Regimen1')
#     print header
#     with open(outfilename, 'w') as f:
#         f.write(','.join([x for x in header]))
#         f.write('\n')
#         #f.close()
#         for i in table:
#             #['Regimen1', 'Regimen2' 'DeltaDir', 'Dir', 'Evi', 'Meta', 'Pref']
#             print "Row: ", i, table[i], '\n'
#             reg1 = i[0]
#             reg2 = i[1]
#             # evi = table[i][0]
#             # pref = table[i][1]
#             # meta = table[i][2]
#             # direct = table[i][3]
#             # deltaDir = table[i][4]
#             line = ','.join([reg1, reg2])
#
#             #print line, '\n'
#             f.write(line)
#             f.write('\n')
#         f.close()
#
#
#
# outputMarkedTable(d4)
# #for i in d4:
# #    print i,d4[i]
#
#The work below outputs a superiority graph, based on averaged results.
#We also need to be able to output a summary of the differences: Print the result under the "base case"
#and the show any differences. This should probably be as a table ?not sure how to show it as a picture?
#What we're interested in is which pref/meta pair makes the most difference
#From here is just printing -  should prbably wrap in a function
# utilityFunctions.ensure_dir('../Output/Compare/')
# compFileName = '../Output/Compare/out1.dot'
# dotout2file = open(compFileName, "w")
# title = ""
# print >>dotout2file, 'digraph superioritygraph {' + '\n'
# print >>dotout2file, "labelloc = t" + "\n" +'\n'
# print >>dotout2file, compareSuperGraph.generateModDotArcs(d4, summaryMode)
# print >>dotout2file, "}"
# print "\n"
# dotout2file.close()
# subprocess.call(['C:/Program Files (x86)/Graphviz2.38/bin/neato', '-Tsvg', '-O', compFileName], shell = False)
########################################
##################################################
##################################################
##################################################
