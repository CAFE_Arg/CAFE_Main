#################################################
#################################################
####### PyMed, Mon 15th October 2012
####### Released under the GPL version 3
####### Matt Williams & Tony Hunter
#################################################

import generateTable
import expandTable
import queryTable
import normalizeTable
import generateSuperGraph
import outputFunctions
import os
import subprocess
import utilityFunctions
import logging as log
import webbrowser
import datetime as dt

log.basicConfig(filename = 'OutputLog.log', level=log.DEBUG)

#################################################
#################################################

#Set options here:
#Remember: Pref2 = Efficacy, Pref1 = Balanced
#metarulesList = ["nonStatSig","StageII","Grade_NotGood","None"]
#Note: Deliberate revision to single options here
print "CWD: ", os.getcwd()

pref = "prefcriterion1"
#Remember: Pref2 = Efficacy, Pref1 = Balanced
rule = "nonStatSig"
#rule = "Grade_NotGood"
#rule = "None"
#rule = "StageII"

filename ='GBM_RRT1.5_MGMTBoth_0.3.csv'
#filename ='LungCRT_3.63.csv'


inputdirectory = '../Input/'
#inputdirectory = "/home/mw/Software/PyMed/Input/"
#C:\Users\mhw\mw\Software\PyMed\Code

inputFileName = inputdirectory + filename
outputdirectory = "../Output"
#outputdirectory = "/home/mw/Software/PyMed/Output"

print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "\n"
print "PYMED - Monday 15th March 2012"
print "Read following spreadsheet", inputFileName
print "Using pref " + pref + " and Meta-rule " + str(rule) + "\n"

log.debug("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
log.debug("PYMED - Monday 15th March 2012")
i = dt.datetime.now()
log.debug ("Run at: " + str(i))
log.debug("Infile is: " + str(inputFileName))
log.debug("Options are pref: " + str(pref) + " and Meta-rule " + str(rule) + "\n")

evdb = generateTable.getmydb(inputFileName)

evdb = expandTable.addriskratio(evdb)

treatments = queryTable.getmyoplist(evdb)

log.debug("Start superiority graph construction")

treatmentpairs = queryTable.getmyoppairs(evdb)
log.debug("Call generateSuperGraph.filterpairs")
selectedpairs = generateSuperGraph.filterpairs(treatmentpairs,evdb)

superResults = generateSuperGraph.makeSuperGraph(selectedpairs,evdb,pref,rule)
log.debug("superResults Done")
log.debug("SuperResults are:" + str(superResults))
OutputName = filename + "_" + pref + "_" + str(rule)
outputFile = "output.html"
dotOutputName = OutputName + '.dot'
dir1 = os.path.dirname(os.getcwd())
outputDirectory = "Output"
dir = os.path.join(dir1, outputDirectory)

#These make the top-level files: A normalised evidence table and a picture
newOutDir = os.path.join(dir, OutputName)
fullDotOutputName = os.path.join(newOutDir, dotOutputName)
utilityFunctions.ensure_dir(newOutDir)
outputFunctions.generateDotFile(inputFileName, dotOutputName, pref, rule, superResults, newOutDir)
outputFunctions.generateNormalisedTables(evdb, outputFile, fullDotOutputName)
log.debug("By this point should have outputs and Dot file")
subprocess.call(['neato', '-Tsvg', '-O', fullDotOutputName], shell = False)
#subprocess.call(['C:/Program Files (x86)/Graphviz2.36/bin/neato', '-Tsvg', '-O', fullDotOutputName], shell = False)
imgOutputName = fullDotOutputName + '.svg'


log.debug("Using dir: " + str(dir) + " as a base.")
log.debug("FDON: " + str(os.path.abspath(fullDotOutputName)))
log.debug("IMG: " + str(imgOutputName))

print "Using dir: ", dir, " as a base."
#print "FDON: ", os.path.abspath(fullDotOutputName)
#print "IMG: ", imgOutputName
###We've made a sub-directory, so now fill it with subtables
outputFunctions.generateSubtables(evdb, selectedpairs, superResults, newOutDir, rule, pref)

#This then opens files
os.startfile(imgOutputName)
os.startfile(outputFile)

#webbrowser.open(imgOutputName)
