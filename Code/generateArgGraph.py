#################################################
#################################################
# Generate Argument Graph
#
# Wednesday 21 December 2011

import generateInductiveArgs
import generateAttacks
import generateMetaArgs
import logging
log = logging.getLogger(__name__)

#################################################
# Identify arguments undefeated by meta arguments
def getArgsUndefeatedByMetaArgs(Pros,Cons,Meta):
    #log.debug("Start Args: " + str(Pros) + str(Cons) +str(Meta))
    args = []
    for z in Pros:
        args.append(z)
    for z in Cons:
        args.append(z)
    outargs = []
    inargs = []
    for m in Meta:
        #print 'Meta: ', m
        outargs.append(m[2])
    inPros = []
    for m in Pros:
        if not(m in outargs):
            inPros.append(m)
    #        print "inPros :" + str(m)
    inCons = []
    for m in Cons:
        if not(m in outargs):
            inCons.append(m)
    return [inPros,inCons]

#################################################
# Identify undefeated arguments
def getUndefeatedArgs(Pros,Cons,Meta,Attacks):
    args = []
    for z in Pros:
        args.append(z)
    for z in Cons:
        args.append(z)
    outargs = []
    inargs = []
    for m in Meta:
        #print "Meta", m
        outargs.append(m[2])
    #log.debug("In getUndefArgs")
    #log.debug("Pros: " +str(Pros) + " Cons: " + str(Cons) + " Metas: " + str(Meta))
    ##for x in args:
    ##    if not(x in outargs):
            #print "still out " + str(x)
        #else:
    ##        inargs.append(x)
            #print "temp in " + str(x)
    ##for a in Attacks:
    ##    if a[0] in inargs:
    ##        outargs.append(a[1])
    #x = 0

    #while x < len(Attacks):
    #    row = Attacks[x]
    #    y = 0
    #    while y < len(row):
    #        item = row[y]
    #        if item == "P" and Pros[x] in inargs:
    #            outargs.append(Cons[y])
    #        elif item == "C" and Cons[y] in inargs:
    #            outargs.append(Pros[x])
    #        y = y+1
    #    x = x+1

    #for m in outargs:
    #    print "Outarg " + str(m)
    inPros = []
    for m in Pros:
        if not(m in outargs):
            inPros.append(m)
    #log.debug("inPros :" + str(inPros))

    inCons = []
    for m in Cons:
        if not(m in outargs):
            inCons.append(m)
    #log.debug("inCons :" + str(inCons))

    if len(inPros) > 0 and len(inCons) > 0:
        x = 0
        while x < len(Attacks):
            if not(Pros[x] in outargs):
                #print "Not yet out " + str(x)
                #print Pros[x]
                #print x
                row = Attacks[x]
                y = 0
                #flags = []
                pflag = "U"
                cflag = "U"
                #print str(row)
                while y < len(row):
                    item = row[y]
                    if item == "P":
                        pflag = "P"
                    elif item == "C" and not(Cons[y] in outargs):
                        cflag = "C"
                    y = y+1
                #print "pflag :" + pflag
                #print "cflag :" + cflag
                if pflag == "P" and cflag == "U":
                    inargs.append(Pros[x])
            x = x+1
        #print "Attacks 0"
        #print Attacks
        if len(Attacks) > 0:
            rowlength = len(Attacks[0])
            y = 0
            while y < rowlength:
                if not(Cons[y] in outargs):
                    #print y
                    x = 0
                    pflag = "U"
                    cflag = "U"
                    while x < len(Attacks):
                        row = Attacks[x]
                        item = row[y]
                        if item == "C":
                            cflag = "C"
                        elif item == "P" and not(Pros[x] in outargs):
                            pflag = "P"
                        x = x+1
                    if pflag == "U" and cflag == "C":
                        inargs.append(Cons[y])
                y = y+1
    elif len(inPros) > 0 and len(inCons) == 0:
        for u in Pros:
            if not(u in outargs):
                inargs.append(u)
    elif len(inPros) == 0 and len(inCons) > 0:
        for u in Cons:
            if not(u in outargs):
                inargs.append(u)

    undefeatedargs = []
    defeatedargs = []

    #for u in args:
        #print u
    #    if not(u in outargs):
    #        undefeatedargs.append(u)
    #    elif u in outargs:
    #        defeatedargs.append(u)

    for u in args:
        #print u
        if u in inargs:
            undefeatedargs.append(u)
        elif not(u in inargs):
            defeatedargs.append(u)

    #log.debug("DefArg: " + str(defeatedargs))
    #log.debug("UndefArg: " + str(undefeatedargs))
    if len(undefeatedargs) > 0:
        if undefeatedargs[0][0] == []: #Then there is no support
            #log.debug("Cleaning up " + str(undefeatedargs))
            undefeatedargs[0][3] = 'equal'
    #log.debug("NewUndefArg: " + str(undefeatedargs))
    return undefeatedargs


#################################################
# Identify directionality from argument graph
def getDirection(Undefeated):
    directions = []
    for u in Undefeated:
        directions.append(u[3])
    mylist = set(directions)
    if len(mylist) == 1:
        return directions[0]
    else:
        return "equal"


#################################################
# Generate argument graph functions
#
# Print statements give the info about the argument graph
#
# Here evtable is the normalized table
# and evdb is the original table

def doArgumentGraph(T1,T2,evtable,pref,evdb,metarules):
    parts = generateInductiveArgs.partition(T1,T2,evtable)
    #log.debug("Starting Arg for " + T1 + " & " + T2)
    #log.debug("Evidence for pro arguments")
    #log.debug(parts[0])
    ##print "\n"
    #log.debug("Evidence for con arguments")
    #log.debug(parts[1])

    if len(parts[0]) >= 1:
        proPowerSet = generateInductiveArgs.powergen(parts[0])
        proargs = generateInductiveArgs.buildargs(proPowerSet,evtable,'sup',T1,T2)
    else:
        proargs = []
    #print "Number of proargs is " + str(len(proargs))
    #for m in proargs:
    #    print m

    #print "Con inductive arguments"
    if len(parts[1]) >= 1:
        conPowerSet = generateInductiveArgs.powergen(parts[1])
        conargs = generateInductiveArgs.buildargs(conPowerSet,evtable,'inf',T1,T2)
    else:
        conargs = []
    #print "Number of conargs is " + str(len(conargs))

    #for m in conargs:
    #    print m
    #print "\n"

    #print "\n"

    ######################################################
    ######################################################
    # print "\n"
    # print "\n"
    # print "Bodgit fix here: Delete all evidence from each argument that fails metarule criterion"
    # print "\n"

    #for arg in proargs:
    #    newarg = []
    #    for item in arg:
    #        for mr in metarules:
    #            if

    bodgitproargs = []
    bodgitconargs = []
    for arg in proargs:
        for item in arg[0]:
            singleton = [[item],arg[1],arg[2],arg[3]]
            #print singleton
            bodgitproargs.append(singleton)
    for arg in conargs:
        for item in arg[0]:
            singleton = [[item],arg[1],arg[2],arg[3]]
            #print singleton
            bodgitconargs.append(singleton)
    #print str(bodgitproargs)
    #print str(bodgitconargs)
    #print "\n"
    #print 'MR: ', metarules
    bodgitmetas = generateMetaArgs.buildmetaargs(bodgitproargs,bodgitconargs,evtable,evdb,metarules)
    #log.debug('BodgitMetas: ' + str(bodgitmetas))
    #print str(bodgitmetas)
    bodgitevidence = []
    #log.debug("Now in GenArgGraph l.250")

#This seems to be a key area for the algorithm
#KEY ARG ALG
    #if bodgitmetas == []: print "No metas"
    for ma in bodgitmetas:
        #print "MA:", ma
        arg = ma[2]
        evidence = arg[0]
        for item in evidence:
            bodgitevidence.append(item)

    for arg in proargs:
        #log.debug("ProArgx: " +str(arg))
        for item in bodgitevidence:
            if item in arg[0]:
                #log.debug("Removing from ProArgs: " + str(item))
                arg[0].remove(item)

    for arg in conargs:
        #log.debug("ConArgx: " +str(arg))
        for item in bodgitevidence:
            if item in arg[0]:
                #log.debug("Removing from ConArgs: " + str(item))
                arg[0].remove(item)

    #log.debug("Revised proarg is " + str(proargs))
    #log.debug("Revised conarg is " + str(conargs))



    # print "Following use of meta-arguments is bypassed because of Bodgit fix above"
    # print "\n"
    # print "Get meta arguments attacking inductive arguments"
    metas = generateMetaArgs.buildmetaargs(proargs,conargs,evtable,evdb,metarules)
    #print 'Metas ', metas
    # print "Number of metaargs is " + str(len(metas))
    # print metas
    # print "\n"
    # print "Remove args defeated by metaargs"
    # print "Calling getArgsUndefeatedByMetaArgs"
    temp = getArgsUndefeatedByMetaArgs(proargs,conargs,metas)
    # print "Temp:", temp
    newproargs = temp[0]
    #print "Number of remaining proargs is " + str(len(newproargs))
    newconargs = temp[1]
    #log.debug("NewProArgs: " + str(newproargs))
    #log.debug("NewConArgs: " + str(newconargs))
    #print "Number of remaining conargs is " + str(len(newconargs))
    #print "\n"
    ##print "Strict attacks for inductive arguments"
    #print "Calling generateAttacks.getStrictAttacks"
    attacks = generateAttacks.getStrictAttacks(newproargs,newconargs,evtable,pref,evdb)
    #print "Number of attacks is " + str(len(attacks))
    #for s in attacks: print s
    #print "\n"
    ##print "Meta arguments attacking inductive arguments"
    ##metas = generateMetaArgs.buildmetaargs(newproargs,newconargs,evtable,evdb,metarules)
    #for m in metas:
    #    print m
    #print "\n"
    #print "Undefeated arguments"
    newmetas = []
    undefeated = getUndefeatedArgs(proargs,conargs,newmetas,attacks)
    #log.debug("Undefs are: " + str(undefeated))
    #for x in undefeated: print x
    #print "\n"
    #print "Superiority result"
    #print "\n"
    direction = getDirection(undefeated)
    s = ""
    if direction == 'sup':
        s = T1 + " is superior to " + T2
    elif direction == 'inf':
        s = T2 + " is superior to " + T1
    else:
        s = T1 + " is equal to " + T2
    #print s
    ##print "\n"
    evidence = parts[2]
    #print "On the basis of:"
    #print evidence, pref, metarules
    metaSet = set()
    for m in bodgitmetas:
        #print 'BM ', m
        metaSet.add(m[1]) #This adds the ID in the meta rules
    #print "MS: ", metaSet

    #print 'MetaSet: ', metaSet
    summary = {}
    summary['T1'] = T1
    summary['T2'] = T2
    summary['Direction'] = direction
    summary['Evidence'] = evidence
    summary['Metaset'] = metaSet
    #print 'MetaSet: ', metaSet
    summary['Pref'] = pref

    # summary = []
    # summary.append(T1)
    # summary.append(T2)
    # summary.append(direction)
    # summary.append(evidence)
    # summary.append(metaSet)
    return summary
