import logging as log

log.basicConfig(filename = 'OutputLog.log', level=log.DEBUG)

def compareDirections(base, newcase):
    """Given two directions, returns the difference in them"""
    #print 'B: ', base, 'N: ', newcase
    assert (type(base) == type(newcase)), (base, newcase, type(base), type(newcase))
    if base =='': raise ValueError(base, newcase)
    if newcase == '': raise ValueError(base, newcase)
    #assert base in ['sup', 'inf', 'equal'], base
    assert newcase in ['sup', 'inf', 'equal'], newcase
    if base == newcase: return 'nodiff'
    elif base == 'sup' and newcase == 'inf': return 'reversaldecrease'
    elif base == 'inf' and newcase == 'sup': return 'reversalincrease'
    elif base == 'equal' and newcase == 'sup': return 'increase'
    elif base == 'equal' and newcase == 'inf': return 'decrease'
    elif base == 'inf' and newcase == 'equal': return 'increaseEq'
    elif base == 'sup' and newcase == 'equal': return 'decreaseEq'
    else: raise ValueError (base, newcase)



def sumDirection(summaryDict, summaryMode):
	"""Given a summary dictionary of superGraph results, \n
	   returns the summarised direction \n
	   Thie summary method is controlled using the summaryMode"""
	if summaryMode == 'simple':
#		assert type(base) == type(newcase) == strx[]
#	assert base in ['sup', 'inf', 'equal']
#	assert newcase in ['sup', 'inf', 'equal']
		total = 0
		for x in summaryDict:
			#print x, summaryDict[x]
			assert x['Dir'] in ['sup', 'inf', 'equal']
			if x['Dir'] == 'sup': total += 1
			if x['Dir'] == 'equal': pass
			if x['Dir'] == 'inf': total -= 1
		return total

# def returnPenWeight(score, normWeight):
# 	calc = float(score)/normweight
# 	if abs(calc) >1 or abs(calc) < 0: raise ValueError
# 	if abs(calc) >= 0.8: return certain
# 	elif abs(calc) >= -0.6 and < 0.8: return modCertain
# 	elif abs(calc) >= 0.35 and < 0.6: return balanced
# 	elif abs(calc) >=0.2 and < 0.35: return lessCertain
# 	elif abs(calc) <0.2: return uncertain

def trueCompTuple(t1,t2):
	"""Compares two tuples in an order independent fashion"""
	#print "Comparing ", t1, t2, "\n"
	assert len(t1) == len(t2)
	assert type(t1) == type(t2)
	#Here we assume tuples are of size 2
	if t1 == t2:
		return True
	elif t1[0] == t2[1] and t1[1] == t2[0]:
		return True
	else: return False

def makeDictofPairs(pairs):
	"""To make the dictionary with the pairs as keys, and ensure keys are unique"""
	d = {}
	for i in pairs:
		d[tuple(i)] = []
	for i in d.keys():
		for j in d.keys():
			if trueCompTuple(i,j) and i <> j:
				d.pop(j, None)
	return d

def directionReverse(direction):
	if direction not in ["sup", "inf", "equal"]:
		raise ValueError
	if direction == "sup": return "inf"
	if direction == "inf": return "sup"
	if direction == "equal": return "equal"

def addSuperGraph(TreatmentPairDict, superGraph):
	"""Expects a dictionary (with pair-tuples as keys), to which it adds the results of this superGraph \n 
		Note that this means the dictionary is mutable"""
	for i in superGraph:
	    for j in TreatmentPairDict.keys():
        	t1 = tuple([i['T1'],i['T2']])
        	t2 = tuple([i['T2'],i['T1']]) #Note reversal here
        	if t1 == j:
        		TreatmentPairDict[j].append({"Meta": i["Metaset"], "Pref": i["Pref"], "Dir": i["Direction"], "Evi": i["Evidence"]})
        		log.debug("Match on " + str(t1) + str(i["Direction"]))
        	elif t2 == j:
        		TreatmentPairDict[j].append({"Meta": i["Metaset"], "Pref": i["Pref"], "Dir": compareSuperGraph.directionReverse(i["Direction"]), "Evi": i["Evidence"]})
        		log.debug("Match inverted " + str(t2) + str(j))    
	return TreatmentPairDict

def markSuperGraphs(TreatmentPairDict, baseMeta, basePref):
	#"""Takes a dictionary with all of the treatment pairs and their outcomes under different options \n
    #and compares them to the base case, and then marks them"""
	#First we find the directions for the base case, and mark that as the base direction
    for i in TreatmentPairDict.keys():
        baseDirection = ""
        for x in TreatmentPairDict[i]:
            if x['Meta'] == baseMeta and x['Pref'] == basePref:
                baseDirection = x['Dir']
                x['DeltaDir'] = 'BASE'
            else:
                x['DeltaDir'] = 'undef'  
    #eviSet = set()
    #for z in TreatmentPairDict[i]: This is about trying to check the evidence is the same all over, as a safety measure
    #    eviSet.add(z['Evi'])    
        for j in TreatmentPairDict[i]:
            if j['DeltaDir'] == 'BASE':
                pass
            else:
                if baseDirection <> '': j['DeltaDir'] = compareDirections(baseDirection, j['Dir'])
                else: j['DeltaDir'] = 'ERROR'
                #raise ValueError (i, j)
    return TreatmentPairDict

def generateModDotArcs(markedTreatments, summaryMode):
    """To generate arcs, but based on the average outcome, and normalised by the \n 
    total number of options"""
    size = len(markedTreatments[markedTreatments.keys()[0]]) #This is how many options we could have
    #Then we need some form of converstion table - i.e. if you get a score of +2/7, what style do you get
    #We assume that this is symmetrical - i.e. +2/7 gets the same as -2/7, but reversed
    outstring = ''
    quotes = '"'
    arc = '"->"'
    l = ' [label = '
    extraRed = ' color="red"];'
    extraBlue = ' color="blue"];'
    endline = "\n"
    styleDotted = ' [style = dotted] '
    styleDashed = ' [style = dashed] '
    certain = ' penwidth=5 ' 
    modCertain = ' penwidth=4 '
    balanced = 'penwidth=3 '
    lessCertain = ' penwidth=2 '
    uncertain = ' penwidth=0.5 '
    for x in markedTreatments:
        summaryDirection = sumDirection(markedTreatments[x], summaryMode)
        #print "SumDir ", summaryDirection
        #normWeight = compareSuperGraph.returnPenWeight(summaryDirection, size)
        calc = float(summaryDirection)/size
        if abs(calc) >1 or abs(calc) < 0: raise ValueError
        if abs(calc) >= 0.8: normWeight = certain
        elif 0.6 < abs(calc) <= 0.8: normWeight =  modCertain
        elif 0.35 < abs(calc) <= 0.6: normWeight =  balanced
        elif 0.2 < abs(calc) <= 0.35: normWeight = lessCertain
        elif abs(calc) <0.2: normWeight =  uncertain
        url = "./" + x[0] + ' ' + x[1] + '.html'
        #print url
        meta = ' '
        labelURL = ' labelURL = "' + url + '"'
        #print "URL: ", url 
        evi =', '.join(markedTreatments[x][0]['Evi'])
        if summaryDirection > 0:
            outstring = outstring + quotes + x[0] + arc + x[1] + quotes + l + quotes + evi + " " + meta + quotes + labelURL + ", " + normWeight + extraRed + endline
        if summaryDirection == 0:
            outstring = outstring + quotes + x[0] + arc + x[1] + quotes + l + quotes + evi + " " + meta + quotes + labelURL + ", " + normWeight + ' dir = "both" ' + extraBlue + endline  
        if summaryDirection < 0:
            outstring = outstring + quotes + x[1] + arc + x[0] + quotes + l + quotes + evi + " " + meta + quotes + labelURL + ", " + normWeight + extraRed + endline

    return outstring

def generateMarkedTable(markedTreatments, summaryMode):
    """Outputs a CSV file, where the rows are the treatment pairs and the columns are the options"""
    for x in markedTreatments:
        print x, markedTreatments[x]
        print '\n'
