#################################################
#################################################
####### PyMed, Mon 15th October 2012
####### Released under the GPL version 3
####### Matt Williams & Tony Hunter
#################################################

import generateTable
import expandTable
import queryTable
import normalizeTable
import generateSuperGraph
import outputFunctions
import os
import subprocess
import utilityFunctions
import logging as log
import webbrowser

log.basicConfig(filename = 'OutputLog.log', level=log.DEBUG)

#################################################
#################################################

#Set options here: 

prefList = ["prefcriterion1", "prefcriterion2"]
metarulesList = ["nonStatSig", "StageII","Grade_NotGood"]
#prefList = ["prefcriterion1"]
#metarulesList = ["nonStatSig"]
#filename ='Lung_CRT_TEST.csv'
filename ='LungCRT_3.21.csv'


#inputdirectory = '../Input/'
inputdirectory = "/home/mw/Software/PyMed/Input/"

inputFileName = inputdirectory + filename
#outputdirectory = "../Output"
outputdirectory = "/home/mw/Software/PyMed/Output"

print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "\n"
print "PYMED - Monday 15th March 2012"
print "Read following spreadsheet", inputFileName
log.debug("\n PYMED - Monday 15th March 2012 \n")

evdb = generateTable.getmydb(inputFileName)

evdb = expandTable.addriskratio(evdb)


treatments = queryTable.getmyoplist(evdb)

print "Start superiority graph construction"

treatmentpairs = queryTable.getmyoppairs(evdb)
#print "Call generateSuperGraph.filterpairs"
selectedpairs = generateSuperGraph.filterpairs(treatmentpairs,evdb)

    
print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "Now iterating over options"
print "Metarules ", metarulesList
print "Prefs ", prefList

log.debug("Metarules: " + str(metarulesList))
log.debug("Prefs: " + str(prefList))


for rule in metarulesList:
    for pref in prefList:
        print rule, pref
        superResults = generateSuperGraph.makeSuperGraph(selectedpairs,evdb,pref,rule)
        log.debug("superResults Done")
        OutputName = filename + "_" + pref + "_" + rule
        outputFile = "output.html"
        dotOutputName = OutputName + '.dot'
        print "This superiority graph was constructed using pref " + pref + " and Meta-rule " + rule + "\n"
        dir1 = os.path.dirname(os.getcwd())
        outputDirectory = "Output"
        dir = os.path.join(dir1, outputDirectory)

        #These make the top-level files: A normalised evidence table and a picture
        newOutDir = os.path.join(dir, OutputName) 
        fullDotOutputName = os.path.join(newOutDir, dotOutputName)
        utilityFunctions.ensure_dir(newOutDir)
        outputFunctions.generateDotFile(inputFileName, dotOutputName, pref, rule, superResults, newOutDir)
        outputFunctions.generateNormalisedTables(evdb, outputFile, fullDotOutputName)
        log.debug("By this point should have outputs and Dot file")
        subprocess.call(['neato', '-Tsvg', '-O', fullDotOutputName], shell = False)
        imgOutputName = fullDotOutputName + '.svg'

        print "Using dir: ", dir, " as a base."
        print "FDON: ", os.path.abspath(fullDotOutputName)
        print "IMG: ", imgOutputName
        ###We've made a sub-directory, so now fill it with subtables
        outputFunctions.generateSubtables(evdb, selectedpairs, superResults, newOutDir, rule, pref)

        #This then opens files
        #os.startfile(imgOutputName)
        #os.startfile(outputFile)
        #webbrowser.open(imgOutputName)
# #################################################
# #################################################
# #################################################
# #################################################

