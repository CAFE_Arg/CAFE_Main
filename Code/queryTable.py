# Functions to get info out of evidence table dictionaries


######################################################
# This function returns the list of myids for
# the evidence table


def getmyidlist(mydatabase):
    myidlist = []
    for x in mydatabase:
        myidlist.append(x['myid'])
        myidlist.sort()
    return myidlist



#######################################################
# This function returns the list of treatment options
# in the evidence table. Duplicates are removed.


def getmyoplist(mydatabase):
    myoplist = []
    for x in mydatabase:
        myoplist.append(x['ArmA_TotalShort'])
        myoplist.append(x['ArmB_TotalShort'])
        myoplist = list(set(myoplist))
        myoplist.sort()
    return myoplist

#######################################################
# This function returns the list of treatment pairs
# for which there is evidence in the evidence table.



def getmyoppairs(mydatabase):
    pairlist = []
    oplist = getmyoplist(mydatabase)
    for x in oplist:
        #oplist.remove(x)
        for y in oplist:
            templist = []
            templist.append(x)
            templist.append(y)
            if not(templist in pairlist):
                pairlist.append(templist)
    for x in pairlist:
        y = reverselist(x)
        if y in pairlist:
            pairlist.remove(y)
    #newpairlist = [list(t) for t in set(tuple(element) for element in pairlist)]         
    return pairlist

#######################################################
# This function returns the subtable of evidence table
# that compare the treatments in the pair

def getmysubtable(t1,t2,mytable):
    subtable = []
    #for x in mytable:
    #    print x['myid']
    for x in mytable:
        if t1 == x['ArmA_TotalShort'] and t2 == x['ArmB_TotalShort']:
            subtable.append(x)
            #print "Adding " + str(x['myid'])
        elif t2 == x["ArmA_TotalShort"] and t1 == x["ArmB_TotalShort"]:
            subtable.append(x)
            #print "Adding " + str(x['myid'])
    return subtable
        


            



#######################################################
# This function returns the reverse of the pair

def reverselist(mypair):
    myreversepair =  []
    myreversepair.append(mypair[1])
    myreversepair.append(mypair[0])    
    return myreversepair



######################################################
