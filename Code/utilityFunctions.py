import os


def ensure_dir(f):
    if not os.path.isdir(f):
        try:
            os.makedirs(f)
            #print "Success: ", f
        except OSError, exc: # Python >2.5
            raise
        
