#CheckValuesPyMed
#Reads in the spreadsheet and shows all of the unique values for
# the important columns - hence letting you spot typos.
import csv, os

print "CWD: ", os.getcwd()
filename ='GBM_RRT1.2.csv'
#filename ='LungCRT_3.63.csv'

inputdirectory = '../Input/'
infile = inputdirectory + filename
outputdirectory = "../Output"

inread = csv.DictReader(open(infile))

indata = []
for i in inread:
	indata.append(i)

#Get the headers:
cols = {}
for i in indata[1].keys():
	if 'ARMA_' in i.upper():
		cols[i] = set()
	if 'ARMB_' in i.upper():
		cols[i] = set()
colList = cols.keys()

#print 'CL:', len(colList), cols

for row in indata:
	for k in row:
		if k in colList:
			#print 'MATCH', k
			try:
				cols[k].add(row[k])
			except AttributeError:
				print 'Error', k

print sorted(colList)
inList = ['ArmA_TotalShort', 'ArmB_TotalShort', 'ArmA_TotalShort_GroupA', 'ArmB_TotalShort_GroupA']
for k in cols:
	if k in inList:
		print k
		for i in sorted(cols[k]):
			print i
		print "\n"
