#GenerateInputFilesPyMed
#Takes the base PyMed input file and auto-magically generates all of the others
#Because doing that is boring and prone to error

#Unlike most of our work, we don't use the CSV reader - just direct edits of the header row.
import csv
import logging as log

log.basicConfig(filename = 'GenerateIPFilesLog.log', level=log.DEBUG)

filename = 'GBM_MGMTPos_0.1.csv'
inputdirectory = '../Input/'
infilename = inputdirectory + filename
#outputdirectory = "../Output"
outputdirectory = '../Input/'
inread = csv.reader(open(infilename))

indata = []
for i in inread:
	indata.append(i)

#Now need to generate new files

target = ('ArmA_TotalShort', 'ArmB_TotalShort')

if 'Lung' in filename:
	NewFileDict = {
		'ChemoTimingRelax': ('ArmA_ChemoTimingRelax','ArmB_ChemoTimingRelax'),
		'ChemoYNRelax': ('ArmA_ChemoYN','ArmB_ChemoYN'),
		'ConcPlatRelax': ('ArmA_ConcPlatRelax','ArmB_ConcPlatRelax'),
		'ConsPlatRelax': ('ArmA_ConsPlatRelax','ArmB_ConsPlatRelax'),
		'IndPlatRelax': ('ArmA_IndPlatRelax','ArmB_IndPlatRelax'),
		'OverallChemoRelax': ('ArmA_OverallChemoRegime','ArmB_OverallChemoRegime'),
		'RTFracRelax': ('ArmA_RTFracRelax','ArmB_RTFracRelax'),
		'SingleCombinedRelax': ('ArmA_SingleCombinedRelax', 'ArmA_SingleCombinedRelax'),
		'SingleConcRelax': ('ArmA_SingleConcRelax','ArmB_SingleConcRelax'),
		'SingleConsRelax': ('ArmA_SingleConsRelax','ArmB_SingleConsRelax'),
		'SingleIndRelax': ('ArmA_SingleIndRelax','ArmB_SingleIndRelax'),
		'AllRelax':('ArmA_TotalRelax','ArmB_TotalRelax')
	}

if 'GBM' in filename:
	NewFileDict = {
		'ChemoYNRelax': ('ArmA_ChemoYN','ArmB_ChemoYN'),
		'GroupA1': ('ArmA_TotalShort_GroupA1','ArmB_TotalShort_GroupA1'),
		'GroupA2': ('ArmA_TotalShort_GroupA2','ArmB_TotalShort_GroupA2'),
		'GroupB': ('ArmA_TotalShort_GroupB', 'ArmB_TotalShort_GroupB'),
		'GroupC': ('ArmA_TotalShort_GroupC', 'ArmB_TotalShort_GroupC')
	}


def switchHeader(infile, indata, switchKeyDict, switchKey, target):
	"""This function switches the column header names, and writes out a new file,
	based on the info given in the switchKey dictionary and the target tuple"""
	#removes the CSV and the ., and adds the key from the switchKeyDict
	outfilename = (infilename[:-4]) + '_' + switchKey + '.csv'
	log.debug("outfilename: " +outfilename)
	#print outfilename
	header = indata[0]
	log.debug("Header: " + str(header))
	firstItem = switchKeyDict[switchKey][0]
	secondItem = switchKeyDict[switchKey][1]
	log.debug("1st Item: " + firstItem + " 2nd Item: " + secondItem)
	newHeader = [x for x in header]
	for i,v in enumerate(newHeader):
		if v == target[0]:
			newHeader.pop(i)
			newHeader.insert(i,'OLD_A')
		if v == target[1]:
			newHeader.pop(i)
			newHeader.insert(i,'OLD_B')
	#We do this in two steps, to avoid over-writing things
	for i,v in enumerate(newHeader):
		if v == firstItem:
			newHeader.pop(i)
			newHeader.insert(i, target[0])
		if v == secondItem:
			newHeader.pop(i)
			newHeader.insert(i,target[1])
	log.debug("SwitchKey: " + switchKey)
	log.debug("Header: " + str(header))
	log.debug("NewHeader: " + str(newHeader))
	print '\n'
	with open(outfilename, 'wb') as outF:
		outfile = csv.writer(outF)
		outfile.writerow(newHeader)
		indexA, indexB = newHeader.index('ArmA_TotalShort'), newHeader.index('ArmB_TotalShort')
		for count, line in enumerate(indata):
			log.debug("Line from inData: " + str(count) + " " + str(line))
			if count == 0: pass
			else:
				if checkDuplicateTreatment(line, indexA, indexB):
					log.debug("Not Writing: " + str(line))
					log.debug("Because:" + line[indexA] + " equals " + line[indexB])
				else:
					log.debug("Writing: " + str(line))
					outfile.writerow(line)
				# log.debug("Writing: " + str(line))
				# outfile.writerow(line)
			# if checkDuplicateTreatment(line, firstItem, secondItem): pass
			# else: outfile.writerow(line)

def checkDuplicateTreatment(line, i1, i2):
	if line[i1] == line[i2]: return True
	else: return False

for k in NewFileDict:
	switchHeader(infilename, indata, NewFileDict, k, target)
