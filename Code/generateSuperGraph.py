#################################################
####### Wednesday 21 December 2011
#################################################

import queryTable
import generateArgGraph
import normalizeTable

#################################################

def makeSuperGraph(pairs,etable,pref,metarules):
    #print "HELLO From generateSuperGraph.makeSuperGraph"
    #myoutfilename = 'supergraphdebug.txt'
    #myFileOutput = open(myoutfilename,'w')
    results = []
    for x in pairs:
        T1 = x[0]
        T2 = x[1]
        #print "Here1: " + T1 + " vs " + T2
        #mystring = str(T1) + " vs " + str(T2)
        #myFileOutput.write( mystring )
        subtable = queryTable.getmysubtable(T1,T2,etable)
        #print "IDs of subtable (myid)"
        #for x in subtable:
        #    print x['myid']
        #print "Normalize table"
        normalTable = normalizeTable.makeTable(subtable)
        #for x in normalTable:
        #    print x
			#print str(x['normID']) + " is " + x['Direction'] + x
            #mystring = str(x['normID']) + " is " + x['Direction']
            #myFileOutput.write( mystring )
        summary = generateArgGraph.doArgumentGraph(T1,T2,normalTable,pref,etable,metarules)
        results.append(summary)
        #print str(summary)
        #mystring = str(summary)
        #myFileOutput.write( mystring )
        #myFileOutput = close()
    return results

def filterpairs(treatmentpairs,etable):
    usefulpairs = []
    for x in treatmentpairs:
        for t in etable:
            if t.get('ArmA_TotalShort','Error') == x[0] and t.get('ArmB_TotalShort','Error') == x[1]:
                if not(x in usefulpairs):
                    usefulpairs.append(x)
            elif t.get('ArmA_TotalShort','Error') == x[1] and t.get('ArmB_TotalShort','Error') == x[0]:
                if not(x in usefulpairs):
                    usefulpairs.append(x)
    return usefulpairs
