#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
# Generate inductive arguments
#
# Thursday 22 December 2011
import logging
log = logging.getLogger(__name__)

# Generate inductive argument functions from normalized table
#
# Here Table is the normalized table


def partition(T1,T2,Table):
    pro = []
    con = []
    evidence = set()
    for x in Table:
        log.debug("X in table is: " + str(x))
        if x['ArmA_TotalShort'] == T1 and x['ArmB_TotalShort'] == T2:
            evidence.add(x['TrialID'])
            if x['Direction'] == "sup":
                pro.append(x['normID'])
            elif x['Direction'] == "inf":
                con.append(x['normID'])
        elif x['ArmA_TotalShort'] == T2 and x['ArmB_TotalShort'] == T1:
            evidence.add(x['TrialID'])
            if x['Direction'] == "sup":
                con.append(x['normID'])
            elif x['Direction'] == "inf":
                pro.append(x['normID'])
        #elif x['Direction'] == "eq":
        #    pro.append(x['normID'])
        #    con.append(x['normID'])
    return (pro,con,evidence)

def powergen2(X):
    sets = []
    empty = []
    sets.append(empty)
    for x in X:
        mynewlist = []
        for s in sets:
            s2 = list(s)
            s2.append(x)
            mynewlist.append(s2)
        sets.extend(mynewlist)
    sets.remove([])
    return sets



def powergen(X):
    sets = [X]
    return sets



#def getevidence(x,Table):
#    for e in Table:
#        if e['myid'] == x:
#        if e[0] == x:
#            return e

def buildargs(Powerset,Table,direction,T1,T2):
    firstset = Powerset[0]
    firstitem = firstset[0]
    myinductiveargs = []
    for myset in Powerset:
        myarg = []
        myarg.append(myset)
        myarg.append(T1)
        myarg.append(T2)
        myarg.append(direction)
        myinductiveargs.append(myarg)
    return myinductiveargs

#################################################
#################################################
