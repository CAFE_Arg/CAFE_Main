#################################################
#################################################
#################################################
#################################################
####### Tuesday 20 December 2011
#################################################
#
# Following imports are temporary for coding module


import generateTable
import expandTable
import queryTable

import logging
log = logging.getLogger(__name__)



#################################################
#
# Functions for normalizeTable.py


def makeTable(table):
    newTable = []
    indicators = ['1YrRiskRatio','2YrRiskRatio','3YrRiskRatio','5YrRiskRatio','MedianOSRiskRatio','ResponseRateRiskRatio','Grd3NeutropeniaRiskRatio','Grd3OesToxRiskRatio','Grd3PneumoToxRiskRatio','ToxicDeathRiskRatio', 'MedianPFSRiskRatio', 'Gr3FatigueRiskRatio', 'Gr3InfRiskRatio','Gr3NVRiskRatio']
    k = 1
    for t in table:
    #Note that t here is a dictionary
        for i in indicators:
            if t.get(i,"NA") != "NA": #This checks for an incorrect Key
                newentry = getnewrow(t,i,k)
                newTable.append(newentry)
                k = k+1
    return newTable


def getnewrow(t,i,k):
    log.debug("Adding new row: " + i )
    new = {}
    new['normID'] = k
    attributes = ['myid','ArmA_TotalShort','ArmB_TotalShort','TrialID','Phase', 'OverallGRADE', 'StageInfo']
    for a in attributes:
        temp = t.get(a,"null")
        if temp != "null":
            new[a] = temp
	if i == '1YrRiskRatio':
		new['Outcome'] = '1YrRiskRatio'
        if t.get('1YrRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('1YrRiskRatioDirection',"NA")
        if t.get('1YrRiskRatio',"NA") != "NA":
            new['Value'] = t.get('1YrRiskRatio',"NA")
        sig = t.get('OS_1_SS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == '2YrRiskRatio':
        new['Outcome'] = '2YrRiskRatio'
        if t.get('2YrRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('2YrRiskRatioDirection',"NA")
        if t.get('2YrRiskRatio',"NA") != "NA":
            new['Value'] = t.get('2YrRiskRatio',"NA")
        sig = t.get('OS_2_SS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == '3YrRiskRatio':
        new['Outcome'] = '3YrRiskRatio'
        if t.get('3YrRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('3YrRiskRatioDirection',"NA")
        if t.get('3YrRiskRatio',"NA") != "NA":
            new['Value'] = t.get('3YrRiskRatio',"NA")
        sig = t.get('OS_3_SS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == '5YrRiskRatio':
        new['Outcome'] = '5YrRiskRatio'
        if t.get('5YrRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('5YrRiskRatioDirection',"NA")
        if t.get('5YrRiskRatio',"NA") != "NA":
            new['Value'] = t.get('5YrRiskRatio',"NA")
        sig = t.get('OS_5_SS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == 'MedianOSRiskRatio':
        new['Outcome'] = 'MedianOSRiskRatio'
        if t.get('MedianOSRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('MedianOSRiskRatioDirection',"NA")
        if t.get('MedianOSRiskRatio',"NA") != "NA":
            new['Value'] = t.get('MedianOSRiskRatio',"NA")
        sig1 = t.get('MedOS_SS',"NA")
        sig2 = t.get('HR_SS',"NA")
        log.debug("Sig1: " + sig1 + " Sig2: " + sig2)
        if sig2 == "NA":
            new['Significance'] = getSigValue(sig1)
        else:
            new['Significance'] = getSigValue(sig2)
    if i == 'ResponseRateRiskRatio':
        new['Outcome'] = 'ResponseRateRiskRatio'
        if t.get('ResponseRateRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('ResponseRateRiskRatioDirection',"NA")
        if t.get('ResponseRateRiskRatio',"NA") != "NA":
            new['Value'] = t.get('ResponseRateRiskRatio',"NA")
        sig = t.get('RR_SS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == 'Grd3NeutropeniaRiskRatio':
        new['Outcome'] = 'Grd3NeutropeniaRiskRatio'
        if t.get('Grd3NeutropeniaRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('Grd3NeutropeniaRiskRatioDirection',"NA")
        if t.get('Grd3NeutropeniaRiskRatio',"NA") != "NA":
            new['Value'] = t.get('Grd3NeutropeniaRiskRatio',"NA")
        sig = t.get('Grd3NeutroSS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == 'Grd3OesToxRiskRatio':
        new['Outcome'] = 'Grd3OesToxRiskRatio'
        if t.get('Grd3OesToxRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('Grd3OesToxRiskRatioDirection',"NA")
        if t.get('Grd3OesToxRiskRatio',"NA") != "NA":
            new['Value'] = t.get('Grd3OesToxRiskRatio',"NA")
        sig = t.get('Grd3OesToxSS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == 'Grd3PneumoToxRiskRatio':
        new['Outcome'] = 'Grd3PneumoToxRiskRatio'
        if t.get('Grd3PneumoToxRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('Grd3PneumoToxRiskRatioDirection',"NA")
        if t.get('Grd3PneumoToxRiskRatio',"NA") != "NA":
            new['Value'] = t.get('Grd3PneumoToxRiskRatio',"NA")
        sig = t.get('Grd3PneumoToxSS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == 'ToxicDeathRiskRatio':
        new['Outcome'] = 'ToxicDeathRiskRatio'
        if t.get('ToxicDeathRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('ToxicDeathRiskRatioDirection',"NA")
        if t.get('ToxicDeathRiskRatio',"NA") != "NA":
            new['Value'] = t.get('ToxicDeathRiskRatio',"NA")
        sig = t.get('ToxDeathSS',"NA")
        new['Significance'] = getSigValue(sig)
########From here are new outcomes
    if i == 'MedianPFSRiskRatio':
        new['Outcome'] = 'MedianPFSRiskRatio'
        if t.get('MedianPFSRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('MedianPFSRiskRatioDirection',"NA")
        if t.get('MedianPFSRiskRatio',"NA") != "NA":
            new['Value'] = t.get('MedianPFSRiskRatio',"NA")
        sig = t.get('HRPFS_SS',"NA")
        log.debug('Generating MedPFSArg')
        log.debug(t.get('MedianPFSRiskRatioDirection','Bob'))
        log.debug("Getting HRPFS_SS: " + sig + str(getSigValue(sig)))
        new['Significance'] = getSigValue(sig)
    if i == 'Gr3FatigueRiskRatio':
        new['Outcome'] = 'Gr3FatigueRiskRatio'
        log.debug('Generating Gr3FatigueRR' + t.get('Gr3FatigueRiskRatioDirection','Bob'))
        log.debug("T: " + str(t))
        if t.get('Gr3FatigueRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('Gr3FatigueRiskRatioDirection',"NA")
        if t.get('Gr3FatigueRiskRatio',"NA") != "NA":
            new['Value'] = t.get('Gr3FatigueRiskRatio',"NA")
        sig = t.get('Grd3FatigueSS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == 'Gr3InfRiskRatio':
        new['Outcome'] = 'Gr3InfRiskRatio'
        if t.get('Gr3InfRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('Gr3InfRiskRatioDirection',"NA")
        if t.get('Gr3InfRiskRatio',"NA") != "NA":
            new['Value'] = t.get('Gr3InfRiskRatio',"NA")
        sig = t.get('Grd3InfSS',"NA")
        new['Significance'] = getSigValue(sig)
    if i == 'Gr3NVRiskRatio':
        new['Outcome'] = 'Gr3NVRiskRatio'
        if t.get('Gr3NVRiskRatioDirection',"NA") != "NA":
            new['Direction'] = t.get('Gr3NVRiskRatioDirection',"NA")
        if t.get('Gr3NVRiskRatio',"NA") != "NA":
            new['Value'] = t.get('Gr3NVRiskRatio',"NA")
        sig = t.get('Grd3NVSS',"NA")
        new['Significance'] = getSigValue(sig)
    return new


def getSigValue(sig):
    outstring ="error"
    s = str(sig)
    if s.startswith("1"):
        outstring = "sig"
    elif sig.startswith("0"):
        outstring = "not_sig"
    else:
        outstring = "null"
    return outstring


#################################################
#################################################
#################################################
#################################################
