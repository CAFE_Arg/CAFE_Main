#################################################
# Thursday 22 December 2011
#################################################
# Generate meta-arguments functions from table
import pdb

def buildmetaargs(pros,cons,normTable,evdb,metarules):
    #print "Start buildmetaargs"
    metacount = 1
    allmetas = []
    count = 1
    #print 'MR: ', metarules
    if metarules == "nonEuropean":
        rule1metas = applyRule1(pros,cons,normTable,evdb,count)
        count = count + len(rule1metas)
        allmetas.extend(rule1metas)
    elif metarules == "SEAsian":
        rule2metas = applyRule2(pros,cons,normTable,evdb,count)
        count = count + len(rule2metas)
        allmetas.extend(rule2metas)
    elif metarules == "US":
        rule3metas = applyRule3(pros,cons,normTable,evdb,count)
        count = count + len(rule3metas)
        allmetas.extend(rule3metas)
    elif metarules == 'nonStatSig':
        rule4metas = applyRule4(pros,cons,normTable,evdb,count)
        count = count + len(rule4metas)
        allmetas.extend(rule4metas)
    elif metarules == "induction":
        rule5metas = applyRule5(pros,cons,normTable,evdb,count)
        count = count + len(rule5metas)
        allmetas.extend(rule5metas)
    elif metarules == "PhaseII":
        rule6metas = applyRule6(pros,cons,normTable,evdb,count)
        count = count + len(rule6metas)
        allmetas.extend(rule6metas)
    elif metarules == "StageII":
        rule7metas = applyRule7(pros,cons,normTable,evdb,count)
        count = count + len(rule7metas)
        allmetas.extend(rule7metas)
    elif metarules == "Grade_NotGood":
        rule8metas = applyRule8(pros,cons,normTable,evdb,count)
        count = count + len(rule8metas)
        allmetas.extend(rule8metas)
    elif metarules == "None":
        pass
    else:
        raise ValueError('MR Error', metarules)
    return allmetas

#################################################
# Each applyRuleX function is a rule for a meta-argument
def applyRule1(pros,cons,normTable,evdb,count):
    nonEuropeanCountry = ["US","Japan","Australia","Korea"]
    allargs = []
    allargs.extend(pros)
    allargs.extend(cons)
    metas = []
    metacount = count
    for arg in allargs:
        for x in arg[0]:
            i = getMyId(x,normTable)
            e = getFullRow(i,evdb)
            geo = e.get('RecruitmentArea','error')
            if geo in nonEuropeanCountry:
                mymeta = []
                metaname = "meta" + str(metacount)
                mymeta.append(metaname)
                mymeta.append("nonEuropean")
                mymeta.append(arg)
                metas.append(mymeta)
                metacount = metacount+1
    return metas

def applyRule2(pros,cons,normTable,evdb,count):
    SEAsianCountry = ["Japan","Korea"]
    allargs = []
    allargs.extend(pros)
    allargs.extend(cons)
    metas = []
    metacount = count
    for arg in allargs:
        for x in arg[0]:
            i = getMyId(x,normTable)
            e = getFullRow(i,evdb)
            geo = e.get('RecruitmentArea','error')
            if geo in SEAsianCountry:
                mymeta = []
                metaname = "meta" + str(metacount)
                mymeta.append(metaname)
                mymeta.append("SEAsian")
                mymeta.append(arg)
                metas.append(mymeta)
                metacount = metacount+1
    return metas

def applyRule3(pros,cons,normTable,evdb,count):
    allargs = []
    allargs.extend(pros)
    allargs.extend(cons)
    metas = []
    metacount = count
    for arg in allargs:
        for x in arg[0]:
            i = getMyId(x,normTable)
            e = getFullRow(i,evdb)
            geo = e.get('RecruitmentArea','error')
            if geo == "US":
                mymeta = []
                metaname = "meta" + str(metacount)
                mymeta.append(metaname)
                mymeta.append("US")
                mymeta.append(arg)
                metas.append(mymeta)
                metacount = metacount+1
    return metas

def applyRule4(pros,cons,normTable,evdb,count):
    allargs = []
    allargs.extend(pros)
    allargs.extend(cons)
    metas = []
    metacount = count
    for arg in allargs:
        for x in arg[0]:
            r = getNormalizedRow(x,normTable)
            #print "r: ", r
            sigflag = r.get('Significance','unknown')
            if sigflag == "not_sig" or sigflag == "unknown" or sigflag == "null":
                mymeta = []
                metaname = "meta" + str(metacount)
                mymeta.append(metaname)
                mymeta.append("nonStatSig")
                mymeta.append(arg)
                metas.append(mymeta)
                metacount = metacount+1
    return metas

def applyRule5(pros,cons,normTable,evdb,count):
    metas = []
    metacount = count
    for arg in pros:
        for x in arg[0]:
            i = getMyId(x,normTable)
            e = getFullRow(i,evdb)
            induction = e.get('InductionChemoRegime_A','error')
            if induction != "None":
                mymeta = []
                metaname = "meta" + str(metacount)
                mymeta.append(metaname)
                mymeta.append("LeftUsesInduction")
                mymeta.append(arg)
                metas.append(mymeta)
                metacount = metacount+1
    for arg in cons:
        for x in arg[0]:
            i = getMyId(x,normTable)
            e = getFullRow(i,evdb)
            induction = e.get('InductionChemoRegime_B','error')
            if induction != "None":
                mymeta = []
                metaname = "meta" + str(metacount)
                mymeta.append(metaname)
                mymeta.append("RightUsesInduction")
                mymeta.append(arg)
                metas.append(mymeta)
                metacount = metacount+1
    return metas

def applyRule6(pros,cons,normTable,evdb,count):
    allargs = []
    allargs.extend(pros)
    allargs.extend(cons)
    metas = []
    metacount = count
    for arg in allargs:
        for x in arg[0]:
            r = getNormalizedRow(x,normTable)
            phaseflag = r.get('Phase','unknown')
            if phaseflag == "2" or phaseflag == "II":
                mymeta = []
                metaname = "meta" + str(metacount)
                mymeta.append(metaname)
                mymeta.append("PhaseII")
                mymeta.append(arg)
                metas.append(mymeta)
                metacount = metacount+1
    return metas

def applyRule7(pros,cons,normTable,evdb,count):
    #pdb.set_trace()
    allargs = []
    allargs.extend(pros)
    allargs.extend(cons)
    metas = []
    metacount = count
    for arg in allargs:
        for x in arg[0]:
            r = getNormalizedRow(x,normTable)
            stageflag = r.get('StageInfo','unknown')
            if stageflag != 'unknown':
                stageflaglist = stageflag.split(",")
                stageflaglist2 = []
                for i in stageflaglist:
                    stageflaglist2.append(i.strip())
                    if 'II' not in stageflaglist2:
                        mymeta = []
                        metaname = "meta" + str(metacount)
                        mymeta.append(metaname)
                        mymeta.append("StageII")
                        mymeta.append(arg)
                        metas.append(mymeta)
                        metacount = metacount+1
    return metas

def applyRule8(pros,cons,normTable,evdb,count):
    allargs = []
    allargs.extend(pros)
    allargs.extend(cons)
    metas = []
    metacount = count
    for arg in allargs:
        for x in arg[0]:
            r = getNormalizedRow(x,normTable)
            GradeFlag = r.get('OverallGRADE','unknown')
            if GradeFlag in ['Moderate', 'Poor', 'Very poor', 'Low', 'Very low']:
                mymeta = []
                metaname = "meta" + str(metacount)
                mymeta.append(metaname)
                mymeta.append("Grade_NotGood")
                mymeta.append(arg)
                metas.append(mymeta)
                metacount = metacount+1
    return metas




#################################################

def getNormalizedRow(x,normTable):
    for r in normTable:
        if r.get('normID','error') == x:
            return r
    return {}

def getFullRow(i,evdb):
    for r in evdb:
        if r.get('myid','error') == i:
            return r
    return {}

def getMyId(x,normTable):
    for t in normTable:
        if t.get('normID','error') == x:
            return t.get('myid','error')
    return 'error'



#################################################
