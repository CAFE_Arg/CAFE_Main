#################################################
#################################################
#################################################
# Thursday 22 December 2011
#################################################
# Generate attacks over inductive arguments
#
# Different preferences are captured by
# different versions of the better function.
#
# better1 prefers the argument with higher number of different favourable outcomes
# where "a postive outcome (e.g. OS) being superior" is favourable
# and "a negative outcome (e.g. toxicity) being inferior" is favourable
#
# better2 prefers the argument with higher number of different favourable outcomes
# that are positive outcomes. I.e. it ignores the toxicity results. 

# Here etable is the normalized table
# and evdb is the original table



def getStrictAttacks(pros,cons,etable,pref,evdb):
    #attackfile = open(tempAttacks.txt,'w')

    #strictAttacks = []

    
    k = 10000
    if k < len(pros):
        m = k
    else:
        m = len(pros)
    if k < len(cons):
        n = k
    else:
        n = len(cons)
       
    matrix = [["0" for col in range(n)] for row in range(m)]



    #print "\n"
    #print "Done matrix"
    #print "\n"

    #y = 0
    #while k < 10:
    #for p in pros:
    #    for c in cons:
    #if len(pros) > 0 and len(cons) > 0:

    z = 0

    x = 0
    while x < m:
        y = 0
        #if x % 100 == 0:
        #    print "GenAttacks"
        while y < n:
            p = pros[x]
            c = cons[y]
            #print "Count is " + str(x) + "and " + str(y)
            bresult = better(p,c,etable,pref,evdb)
            #print bresult
            if bresult == "probetter":
                #attack = []
                #attack.append(p)
                #attack.append(c)
                #attack.append(x)
                #attack.append(y)
                #strictAttacks.append(attack)
                #print attack
                #print "Pro is " + str(p)
                #print "Con is " + str(c)
                #print "Probetter"
                matrix[x][y] = "P"
            if bresult == "conbetter":
                #attack = []
                #attack.append(c)
                #attack.append(p)
                #attack.append(y)
                #attack.append(x)
                #strictAttacks.append(attack)
                #print "Pro is " + str(p)
                #print "Con is " + str(c)
                #print "Conbetter"
                #print attack
                matrix[x][y] = "C"
            z = z+1
            y = y+1
        x = x+1
    #print "Number of pros is " + str(len(pros))
    #print "Number of cons is " + str(len(cons))
    #print "Number of attacks is " + str(len(strictAttacks))
    #print "Number of cyles of getStrictAttacks is " + str(z)
    #for row in matrix:
    #    print row
    return matrix

#################################################


def better(p,c,etable,pref,evdb):
    outstring = ""
    if pref == "prefcriterion1":
        outstring = better1(p,c,etable,evdb)
    if pref == "prefcriterion2":
        outstring = better2(p,c,etable,evdb)
    if pref == "prefcriterion3":
        outstring = better3(p,c,etable,evdb)
    return outstring

#################################################

def better1(p,c,etable,evdb):
    pq = qualities1(p,etable)
    cq = qualities1(c,etable)
    #print "Positive qualities " + str(pq)
    #print "Negative qualities " + str(cq)
    
    if len(pq) > len(cq):
        return "probetter"
    elif len(pq) < len(cq):
        return "conbetter"
    else:
        return "same"

def better2(p,c,etable,evdb):
    pq = qualities2(p,etable)
    #print str(pq)
    cq = qualities2(c,etable)
    #print str(cq)
    if len(pq) > len(cq):
        return "probetter"
    elif len(pq) < len(cq):
        return "conbetter"
    else:
        return "same"

def better3(p,c,etable,evdb):
    pFlag = deathRateFlag(p,etable,evdb)
    cFlag = deathRateFlag(c,etable,evdb)
    if pFlag != "high" and cFlag == "high":
        return "probetter"
    elif pFlag == "high" and cFlag != "high":
        return "conbetter"
    elif pFlag == "high" and cFlag == "high":
        return "same"
    elif pFlag != "high" and cFlag != "high":
        return better1(p,c,etable,evdb)


        
#################################################
    

def qualities1(argument,etable):
    qualities = []
    normIDs = argument[0]
    for i in normIDs:
        for r in etable:
            if r["normID"] == i:
                outcomeIndicator = r.get("Outcome","Error")
                if outcomeIndicator != "Error":
                    qualities.append(outcomeIndicator)
    return qualities

def qualities2(argument,etable):
    nonToxIndicators = ['1YrRiskRatio','2YrRiskRatio','3YrRiskRatio','5YrRiskRatio','MedianOSRiskRatio','ResponseRateRiskRatio']
    qualities = []
    normIDs = argument[0]
    for i in normIDs:
        for r in etable:
            if r["normID"] == i:
                outcomeIndicator = r.get("Outcome","Error")
                if outcomeIndicator in nonToxIndicators:
                    qualities.append(outcomeIndicator)
    return qualities

    
def qualities4(argument,etable):
    nonToxIndicators = ['1YrRiskRatio','2YrRiskRatio','3YrRiskRatio','5YrRiskRatio','MedianOSRiskRatio','ResponseRateRiskRatio']
    qualities = []
    normIDs = argument[0]
    for i in normIDs:
        for r in etable:
            if r["normID"] == i:
                outcomeIndicator = r.get("Outcome","Error")
                if outcomeIndicator in nonToxIndicators:
                    qualities.append(outcomeIndicator)
    return qualities

    
    
    
def deathRateFlag(argument,etable,evdb):
    flag = "unknown"
    deathRate = 0
    normIDs = argument[0]
    for i in normIDs:
        for r in etable:
            for t in evdb:
                if r["normID"] == i and r["myid"] == t["myid"]:
                    if argument[3] == "sup":
                        deathRate = r.get("ToxicDeathRate_A","Error")
                    elif argument[3] == "inf":
                        deathRate = r.get("ToxicDeathRate_B","Error")
    if deathRate > 0.01:
        flag = "high"
    return flag
  
    
#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
#################################################
