###############################################################
###############################################################
###############################################################
# Functions to do analysis of evidence table
# and then add columns to evidence table
# Friday 16 Dec

###############################################################
# This function does the risk ration calculations for the
# measured outcomes and adds them to the end of each
# dictionary of the evidence table

import logging
log = logging.getLogger(__name__)

def addriskratio(db1):
    mydb2 = []
    for x in db1:
        #print x
        rq1 = "NA"
        rq2 = "NA"
        rq3 = "NA"
        rq5 = "NA"
        tqd = "NA"
        tq1 = "NA"
        tq2 = "NA"
        tq3 = "NA"
        mq1 = "NA"
        mq2 = "NA"
        mpfsq = "NA"
        g3fatq = "NA"
        g3infq = "NA"
        g3nvq = "NA"
        #log.debug("CalcRatios: " + str(x))
        if x['1yrOS_A'] != 'NA' and x['1yrOS_B'] != 'NA':
            #log.debug("Doing 1yrOS")
            #log.debug(str(x))
            r1 = doratio(x['1yrOS_A'],x['1yrOS_B'])
            if r1 > 1:
                rq1 = "sup"
            elif r1 < 1:
                rq1 = "inf"
            else:
                rq1 = "eq"
            r1 = str(round(r1,4))
            x['1YrRiskRatio'] = r1
        x['1YrRiskRatioDirection'] = rq1

        if x['2yrOS_A'] != 'NA' and x['2yrOS_B'] != 'NA':
            r2 = doratio(x['2yrOS_A'],x['2yrOS_B'])
            if r2 > 1:
                rq2 = "sup"
            elif r2 < 1:
                rq2 = "inf"
            else:
                rq2 = "eq"
            r2 = str(round(r2,4))
            x['2YrRiskRatio'] = r2
        x['2YrRiskRatioDirection'] = rq2

        if x['3yrOS_A'] != 'NA' and x['3yrOS_B'] != 'NA':
            r3 = doratio(x['3yrOS_A'], x['3yrOS_B'])
            if r3 > 1:
                rq3 = "sup"
            elif r3 < 1:
                rq3 = "inf"
            else:
                rq3 = "eq"
            r3 = str(round(r3,4))
            x['3YrRiskRatio'] = r3
        x['3YrRiskRatioDirection'] = rq3

        if x['5YrOS_A'] != 'NA' and x['5YrOS_B'] != 'NA':
            r5 = doratio(x['5YrOS_A'],x['5YrOS_B'])
            if r5 > 1:
                rq5 = "sup"
            elif r5 < 1:
                rq5 = "inf"
            else:
                rq5 = "eq"
            r5 = str(round(r5,4))
            x['5YrRiskRatio'] = r5
        x['5YrRiskRatioDirection'] = rq5

        if x['MedianOS_A'] != 'NA' and x['MedianOS_B'] != 'NA':
            m1 = doratio(x['MedianOS_A'],x['MedianOS_B'])
            if m1 > 1:
                mq1 = "sup"
            elif m1 < 1:
                mq1 = "inf"
            else:
                mq1 = "eq"
            m1 = str(round(m1,4))
            x['MedianOSRiskRatio'] = m1
        x['MedianOSRiskRatioDirection'] = mq1
        log.debug("Doing MedianOSRR: " +m1 + " " + mq1)

        if x['ResponseRate_A'] != 'NA' and x['ResponseRate_B'] != 'NA':
            m2 = doratio(x['ResponseRate_A'],x['ResponseRate_B'])
            if m2 > 1:
                mq2 = "sup"
            elif m2 < 1:
                mq2 = "inf"
            else:
                mq2 = "eq"
            m2 = str(round(m2,4))
            x['ResponseRateRiskRatio'] = m2
        x['ResponseRateRiskRatioDirection'] = mq2

        if x['ToxicDeathRate_A'] != 'NA' and x['ToxicDeathRate_B'] != 'NA':
            try:
                td = doratio(x['ToxicDeathRate_A'],x['ToxicDeathRate_B'])
                if td > 1:
                    tqd = "inf"
                elif td < 1:
                    tqd = "sup"
                else:
                    tqd = "eq"
                td = str(round(td,4))
                x['ToxicDeathRiskRatio'] = td
                x['ToxicDeathRiskRatioDirection'] = tqd
            except ZeroDivisionError:
                x['ToxicDeathRiskRatio'] = "NA"
                x['ToxicDeathRiskRatioDirection'] = "eq"

        if x['Grd3Neutropenia_A'] != 'NA' and x['Grd3Neutropenia_B'] != 'NA':
            #t1 = doratio(float(myclean(x['Grd3Neutropenia_A'])),float(myclean(x['Grd3Neutropenia_B'])))
            t1 = doratio(x['Grd3Neutropenia_A'],x['Grd3Neutropenia_B'])
            #print t1
            if t1 > 1:
                tq1 = "inf"
            elif t1 < 1:
                tq1 = "sup"
            else:
                tq1 = "eq"
            t1 = str(round(t1,4))
            x['Grd3NeutropeniaRiskRatio'] = t1
        x['Grd3NeutropeniaRiskRatioDirection'] = tq1

        if x['Grd3OesTox_A'] != 'NA' and x['Grd3OesTox_B'] != 'NA':
            #t1 = doratio(float(myclean(x['Grd3Neutropenia_A'])),float(myclean(x['Grd3Neutropenia_B'])))
            t2 = doratio(x['Grd3OesTox_A'],x['Grd3OesTox_B'])
            #print t1
            if t2 > 1:
                tq2 = "inf"
            elif t2 < 1:
                tq2 = "sup"
            else:
                tq2 = "eq"
            t2 = str(round(t2,4))
            x['Grd3OesToxRiskRatio'] = t2
        x['Grd3OesToxRiskRatioDirection'] = tq2

        if x['Grd3PneumoTox_A'] != 'NA' and x['Grd3PneumoTox_B'] != 'NA':
            #t1 = doratio(float(myclean(x['Grd3Neutropenia_A'])),float(myclean(x['Grd3Neutropenia_B'])))
            t3 = doratio(x['Grd3PneumoTox_A'],x['Grd3PneumoTox_B'])
            #print t1
            if t3 > 1:
                tq3 = "inf"
            elif t3 < 1:
                tq3 = "sup"
            else:
                tq3 = "eq"
            t3 = str(round(t3,4))
            x['Grd3PneumoToxRiskRatio'] = t3
        x['Grd3PneumoToxRiskRatioDirection'] = tq3

###############This bit is new
###############Added MedianPFS
        #log.debug("XKeys are: " + str(x.keys()))
        if 'MedianPFS_A' in x.keys() and x['MedianPFS_A'] != 'NA' and x['MedianPFS_B'] != 'NA':
            a = doratio(x['MedianPFS_A'],x['MedianPFS_B'])
            log.debug("Generating PFS arg: " + str(x))
            if a > 1:
                mpfsq = "sup"
            elif a < 1:
                mpfsq = "inf"
            else:
                mpfsq = "eq"
            pfs = str(round(a,4))
            #log.debug("MedianPFSRatio: " + str(a) + " " + str(mpfsq) + x['TrialID'])
            x['MedianPFSRiskRatio'] = pfs
            x['MedianPFSRiskRatioDirection'] = mpfsq

        if 'Gr3Fatigue_A' in x.keys() and x['Gr3Fatigue_A'] != 'NA' and x['Gr3Fatigue_B'] != 'NA':
            a = doratio(x['Gr3Fatigue_A'],x['Gr3Fatigue_B'])
            log.debug("Generating Gr3Fatigue arg: " + str(x))
            if a > 1:
                g3fatq = "inf"
            elif a < 1:
                g3fatq = "sup"
            else:
                g3fatq = "eq"
            g3fat = str(round(a,4))
            log.debug("g3fat: " + str(g3fatq))
            x['Gr3FatigueRiskRatio'] = g3fat
            x['Gr3FatigueRiskRatioDirection'] = g3fatq

        if 'Gr3NauseaVom_A' in x.keys() and x['Gr3NauseaVom_A'] != 'NA' and x['Gr3NauseaVom_B'] != 'NA':
            a = doratio(x['Gr3NauseaVom_A'],x['Gr3NauseaVom_B'])
            log.debug("Generating Gr3NausVom arg: " + str(x))
            if a > 1:
                g3nvq = "inf"
            elif a < 1:
                g3nvq = "sup"
            else:
                g3nvq = "eq"
            g3nv = str(round(a,4))
            log.debug("g3nv: " + str(g3nvq))
            x['Gr3NVRiskRatio'] = g3nv
            x['Gr3NVRiskRatioDirection'] = g3nvq

        if 'Gr3Infection_A' in x.keys() and x['Gr3Infection_A'] != 'NA' and x['Gr3Infection_B'] != 'NA':
            a = doratio(x['Gr3Infection_A'],x['Gr3Infection_B'])
            log.debug("Generating Gr3Inf arg: " + str(x))
            if a > 1:
                g3infq = "inf"
            elif a < 1:
                g3infq = "sup"
            else:
                g3infq = "eq"
            g3inf = str(round(a,4))
            log.debug("g3inf: " + str(g3infq))
            x['Gr3InfRiskRatio'] = g3inf
            x['Gr3InfRiskRatioDirection'] = g3infq
    #log.debug("Appending: " + str(x))
        mydb2.append(x)
    return mydb2
        #print x


def doratio(a,b):
    x = myclean(a)
    y = myclean(b)
    log.debug("x: " + str(x))
    log.debug("y: " + str(y))
    if x != 0 and y != 0:
        return x/y
    if x != 0 and y == 0:
        return 10.0
    if x == 0 and y != 0:
        return 0.0
    if x == 0 and y == 0:
        return 1.0

def myclean(x):
    if "%" in x:
        y = x[0:2]
        y = float(y)/100
    else:
        try:
            y = float(x)
            return y
        except ValueError:
            print("Error with trying to convert" + x)


#########################################
#########################################
