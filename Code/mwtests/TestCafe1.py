######################################
#PyMed Test Suite
#Developed from runHere.py on 6th Nov 2016
#####################################

import generateTable
import expandTable
import queryTable
import normalizeTable
import generateSuperGraph
import outputFunctions
import os
import subprocess
import utilityFunctions
import logging as log
import webbrowser
import datetime as dt

log.basicConfig(filename = 'TestOutputLog.log', level=log.DEBUG)

#Set options here:
#Remember: Pref2 = Efficacy, Pref1 = Balanced
#metarulesList = ["nonStatSig","StageII","Grade_NotGood","None"]
#Note: Deliberate revision to single options here
print "CWD: ", os.getcwd()
pref = "prefcriterion1"
rule = "nonStatSig"
#rule = "StageII"
#Remember: Pref2 = Efficacy, Pref1 = Balanced
#rule = "nonStatSig"
#rule = "Grade_NotGood"
#rule = "StageII"
#rule = "Grade_NotGood"
rule = "None"

#filename = 'GBM_Data1_TestD.csv'
#filename ='LungCRT_3.64.csv'

#filename = 'GBM_Data1_TestE.csv'
filename ='GBM_RRT1.2.csv'
inputdirectory = '../Input/'
inputFileName = inputdirectory + filename
outputdirectory = "../Output"

#rule = "None"

#filename = 'GBM_MGMTBGroupA1.csv'
filename = "GBM_Data1_TestB.csv"
#filename = 'GBM_RRT1.5_MGMTBoth_0.3_GroupC.csv'
#filename ='GBM_Data1_TestG.csv'
inputdirectory = '../TestInput/'
#inputdirectory = '../Input/'
inputFileName = inputdirectory + filename
outputdirectory = "../TestOutput"
#outputdirectory = "../Output"

print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
print "TestCafe - 6th Nov 2016"
print "Read following spreadsheet", inputFileName
print "Using pref " + pref + " and Meta-rule " + str(rule) + "\n"

log.debug("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
log.debug("TestCafe - Monday 15th March 2012")
i = dt.datetime.now()
log.debug ("Run at: " + str(i))
log.debug("Infile is: " + str(inputFileName))
log.debug("Options are pref: " + str(pref) + " and Meta-rule " + str(rule) + "\n")

#This is the same as runHere.py
evdb = generateTable.getmydb(inputFileName)
evdb = expandTable.addriskratio(evdb)
treatments = queryTable.getmyoplist(evdb)
log.debug("Start superiority graph construction")
treatmentpairs = queryTable.getmyoppairs(evdb)
log.debug("Call generateSuperGraph.filterpairs")
selectedpairs = generateSuperGraph.filterpairs(treatmentpairs,evdb)
superResults = generateSuperGraph.makeSuperGraph(selectedpairs,evdb,pref,rule)
log.debug("superResults Done")
log.debug("SuperResults are:" + str(superResults))
#This is now new
#rule = "nonStatSig";#rule = "StageII";#Remember: Pref2 = Efficacy, Pref1 = Balanced;#rule = "nonStatSig"
expectedSuper = []
if filename =='GBM_Data1_TestA.csv':
    if rule == "Grade_NotGood" or rule == "StageII":
        log.debug("Using " + rule + "; Neither fire, and therefore outcomes based on prefs")
        if pref == "prefcriterion1":
            expectedSuper = [{'Direction': 'inf', 'Metaset': set([]), 'Pref': 'prefcriterion1', 'T2': 'RT alone; 60 in 30-33', 'Evidence': set(['WickWLancetOncol2012']), 'T1': 'Tem alone'}]
        if pref == "prefcriterion2" :
            expectedSuper = [{'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion2', 'T2': 'RT alone; 60 in 30-33', 'Evidence': set(['WickWLancetOncol2012']), 'T1': 'Tem alone'}]
    if rule == "nonStatSig":
        log.debug("Using nonStatSig; Only applies to the side-effects")
        if pref == "prefcriterion1":
            expectedSuper = [{'Direction': 'sup', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'RT alone; 60 in 30-33', 'Evidence': set(['WickWLancetOncol2012']), 'T1': 'Tem alone'}]
        if pref == "prefcriterion2":
            expectedSuper = [{'Direction': 'sup', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'RT alone; 60 in 30-33', 'Evidence': set(['WickWLancetOncol2012']), 'T1': 'Tem alone'}]

if filename =='GBM_Data1_TestB.csv':
    if rule == "Grade_NotGood":
        log.debug("Using Grade not good - applies to one of the trials")
        if pref == "prefcriterion1":
            expectedSuper = [{'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion1', 'T2': 'TemAloneVar1', 'Evidence': set(['TestSampleA']), 'T1': 'RT alone; 60 in 30-33'}, {'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion1', 'T2': 'RT alone; 60 in 30-33', 'Evidence': set(['WickWLancetOncol2012']), 'T1': 'Tem alone'}]
        if pref == "prefcriterion2" :
            expectedSuper = [{'Direction': 'equal', 'Metaset': set([]), 'Pref': 'prefcriterion2', 'T2': 'TemAloneVar1', 'Evidence': set(['TestSampleA']), 'T1': 'RT alone; 60 in 30-33'}, {'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion2', 'T2': 'RT alone; 60 in 30-33', 'Evidence': set(['WickWLancetOncol2012']), 'T1': 'Tem alone'}]
    if rule == "nonStatSig":
        log.debug("Using nonStatSig - applies to side-effects")
        if pref == "prefcriterion1":
            expectedSuper = [{'Direction': 'inf', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'TemAloneVar1', 'Evidence': set(['TestSampleA']), 'T1': 'RT alone; 60 in 30-33'}, {'Direction': 'sup', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'RT alone; 60 in 30-33', 'Evidence': set(['WickWLancetOncol2012']), 'T1': 'Tem alone'}]
        if pref == "prefcriterion2":
            expectedSuper = [{'Direction': 'inf', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'TemAloneVar1', 'Evidence': set(['TestSampleA']), 'T1': 'RT alone; 60 in 30-33'}, {'Direction': 'sup', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'RT alone; 60 in 30-33', 'Evidence': set(['WickWLancetOncol2012']), 'T1': 'Tem alone'}]
    elif expectedSuper ==[]:
        print("No test coverage for: " + pref + str(rule))

# if filename =='GBM_Data1_TestD.csv':
#     if rule == "Grade_NotGood":
#         if pref == "prefcriterion1":
#             expectedSuper = [{'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion1', 'T2': 'Adj TMZ; 45-60 in 23-33', 'Evidence': set(['WangXAnnSurgOncol2014']), 'T1': 'Adj mCCNU; 45-60 in 23-33'}, {'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ Adj TMZ; 60 in 30', 'Evidence': set(['ElinzanoHAmJClinOncol2015']), 'T1': 'Conc Pac Adj TMZ; 60 in 30'}, {'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ 100 Adj Ext TMZ (B); 60 in 30', 'Evidence': set(['ChoDYWorldNeurosurg2012']), 'T1': 'Conc TMZ 100 Adj Ext TMZ (B) DCV; 60 in 30'}, {'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ Adj TMZ DD 50 CRA 100; 60 in 30', 'Evidence': set(['ClarkeJLJClinOncol2009']), 'T1': 'Conc TMZ Adj TMZ DD 150 CRA 100; 60 in 30'}, {'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion1', 'T2': 'RT alone; 60 in 30', 'Evidence': set(['StuppRLancetOncol2009']), 'T1': 'Conc TMZ Adj TMZ; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ; 60 in 30', 'Evidence': set(['StragliottoGIntJCancer2013']), 'T1': 'Conc TMZ Adj Val; 60 in 30'}, {'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ; 60 in 30', 'Evidence': set(['GaberMCancerManagRes2013']), 'T1': 'Conc TMZ Int; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion1', 'T2': 'Conc Tamox; 56-60 in 28-30', 'Evidence': set(['ZhouSBAsianPacJCancerPrev2015']), 'T1': 'Conc TMZ; 56-60 in 28-30'}]
#         if pref == "prefcriterion2" :
#             expectedSuper = [{'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion2', 'T2': 'Adj TMZ; 45-60 in 23-33', 'Evidence': set(['WangXAnnSurgOncol2014']), 'T1': 'Adj mCCNU; 45-60 in 23-33'}, {'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ Adj TMZ; 60 in 30', 'Evidence': set(['ElinzanoHAmJClinOncol2015']), 'T1': 'Conc Pac Adj TMZ; 60 in 30'}, {'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ 100 Adj Ext TMZ (B); 60 in 30', 'Evidence': set(['ChoDYWorldNeurosurg2012']), 'T1': 'Conc TMZ 100 Adj Ext TMZ (B) DCV; 60 in 30'}, {'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ Adj TMZ DD 50 CRA 100; 60 in 30', 'Evidence': set(['ClarkeJLJClinOncol2009']), 'T1': 'Conc TMZ Adj TMZ DD 150 CRA 100; 60 in 30'}, {'Direction': 'sup', 'Metaset': set([]), 'Pref': 'prefcriterion2', 'T2': 'RT alone; 60 in 30', 'Evidence': set(['StuppRLancetOncol2009']), 'T1': 'Conc TMZ Adj TMZ; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ; 60 in 30', 'Evidence': set(['StragliottoGIntJCancer2013']), 'T1': 'Conc TMZ Adj Val; 60 in 30'}, {'Direction': 'equal', 'Metaset': set([]), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ; 60 in 30', 'Evidence': set(['GaberMCancerManagRes2013']), 'T1': 'Conc TMZ Int; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['Grade_NotGood']), 'Pref': 'prefcriterion2', 'T2': 'Conc Tamox; 56-60 in 28-30', 'Evidence': set(['ZhouSBAsianPacJCancerPrev2015']), 'T1': 'Conc TMZ; 56-60 in 28-30'}]
#     if rule == "nonStatSig":
#         log.debug("Using nonStatSig - applies to side-effects")
#         if pref == "prefcriterion1":
#             expectedSuper = [{'Direction': 'inf', 'Metaset': set([]), 'Pref': 'prefcriterion1', 'T2': 'Adj TMZ; 45-60 in 23-33', 'Evidence': set(['WangXAnnSurgOncol2014']), 'T1': 'Adj mCCNU; 45-60 in 23-33'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ Adj TMZ; 60 in 30', 'Evidence': set(['ElinzanoHAmJClinOncol2015']), 'T1': 'Conc Pac Adj TMZ; 60 in 30'}, {'Direction': 'sup', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ 100 Adj Ext TMZ (B); 60 in 30', 'Evidence': set(['ChoDYWorldNeurosurg2012']), 'T1': 'Conc TMZ 100 Adj Ext TMZ (B) DCV; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ Adj TMZ DD 50 CRA 100; 60 in 30', 'Evidence': set(['ClarkeJLJClinOncol2009']), 'T1': 'Conc TMZ Adj TMZ DD 150 CRA 100; 60 in 30'}, {'Direction': 'sup', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'RT alone; 60 in 30', 'Evidence': set(['StuppRLancetOncol2009']), 'T1': 'Conc TMZ Adj TMZ; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ; 60 in 30', 'Evidence': set(['StragliottoGIntJCancer2013']), 'T1': 'Conc TMZ Adj Val; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'Conc TMZ; 60 in 30', 'Evidence': set(['GaberMCancerManagRes2013']), 'T1': 'Conc TMZ Int; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion1', 'T2': 'Conc Tamox; 56-60 in 28-30', 'Evidence': set(['ZhouSBAsianPacJCancerPrev2015']), 'T1': 'Conc TMZ; 56-60 in 28-30'}]
#         if pref == "prefcriterion2":
#             expectedSuper = [{'Direction': 'inf', 'Metaset': set([]), 'Pref': 'prefcriterion2', 'T2': 'Adj TMZ; 45-60 in 23-33', 'Evidence': set(['WangXAnnSurgOncol2014']), 'T1': 'Adj mCCNU; 45-60 in 23-33'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ Adj TMZ; 60 in 30', 'Evidence': set(['ElinzanoHAmJClinOncol2015']), 'T1': 'Conc Pac Adj TMZ; 60 in 30'}, {'Direction': 'sup', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ 100 Adj Ext TMZ (B); 60 in 30', 'Evidence': set(['ChoDYWorldNeurosurg2012']), 'T1': 'Conc TMZ 100 Adj Ext TMZ (B) DCV; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ Adj TMZ DD 50 CRA 100; 60 in 30', 'Evidence': set(['ClarkeJLJClinOncol2009']), 'T1': 'Conc TMZ Adj TMZ DD 150 CRA 100; 60 in 30'}, {'Direction': 'sup', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'RT alone; 60 in 30', 'Evidence': set(['StuppRLancetOncol2009']), 'T1': 'Conc TMZ Adj TMZ; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ; 60 in 30', 'Evidence': set(['StragliottoGIntJCancer2013']), 'T1': 'Conc TMZ Adj Val; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'Conc TMZ; 60 in 30', 'Evidence': set(['GaberMCancerManagRes2013']), 'T1': 'Conc TMZ Int; 60 in 30'}, {'Direction': 'equal', 'Metaset': set(['nonStatSig']), 'Pref': 'prefcriterion2', 'T2': 'Conc Tamox; 56-60 in 28-30', 'Evidence': set(['ZhouSBAsianPacJCancerPrev2015']), 'T1': 'Conc TMZ; 56-60 in 28-30'}]
#     else:
#         expectedSuper = []
#         print("No test coverage for: " + pref + str(rule))

try:
    assert(superResults == expectedSuper)
    print "Passed: ", rule, pref
    log.debug("Passed Test: " + str(rule) + pref)
except AssertionError:
    log.warning("ERROR with: " + pref + rule + " Actual & Exp below")
    log.debug("Actual: " + str(superResults))
    log.debug("Expect: " + str(expectedSuper))



#This is then the same as runHere.py
OutputName = filename + "_" + pref + "_" + str(rule)
OutputFilename = "SummaryTable_" + filename + ".html"
dotOutputName = OutputName + '.dot'
dir1 = os.path.dirname(os.getcwd())
outputDirectory = 'Output/'
dir = os.path.join(dir1, outputDirectory)
outputSummaryTable = os.path.join(dir, OutputFilename)
print(outputSummaryTable)

#These make the top-level files: A normalised evidence table and a picture
newOutDir = os.path.join(dir, OutputName)
fullDotOutputName = os.path.join(newOutDir, dotOutputName)
utilityFunctions.ensure_dir(newOutDir)
outputFunctions.generateDotFile(inputFileName, dotOutputName, pref, rule, superResults, newOutDir)
outputFunctions.generateNormalisedTables(evdb, outputSummaryTable, fullDotOutputName)
log.debug("By this point should have outputs and Dot file")
subprocess.call(['neato', '-Tsvg', '-O', fullDotOutputName], shell = False)
#subprocess.call(['C:/Program Files (x86)/Graphviz2.38/bin/neato', '-Tsvg', '-O', fullDotOutputName], shell = False)
imgOutputName = fullDotOutputName + '.svg'

log.debug("Using dir: " + str(dir) + " as a base.")
log.debug("FDON: " + str(os.path.abspath(fullDotOutputName)))
log.debug("IMG: " + str(imgOutputName))
###We've made a sub-directory, so now fill it with subtables
outputFunctions.generateSubtables(evdb, selectedpairs, superResults, newOutDir, rule, pref)

#This then opens files
os.startfile(imgOutputName)
os.startfile(outputSummaryTable)
#webbrowser.open(imgOutputName)
# #################################################
# #################################################
# #################################################
# #################################################
