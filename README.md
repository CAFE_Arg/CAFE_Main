This is the readme file for the CAFE project

Matt Williams and Tony Hunter (2012)

All software released under GPL v.3: http://www.gnu.org/copyleft/gpl.html
All documentation released under Creative Commons Attribution-NoDerivatives 4.0 International License, except for those elements where copyright lies with external publishers.


Matt Williams: http://www.imperial.ac.uk/people/matthew.williams matthew.williams2-at-imperial.nhs.uk	
Tony Hunter: http://www0.cs.ucl.ac.uk/staff/a.hunter/ anthony.hunter-at-ucl.ac.uk

Further contact details are available through our webpages

CAFE is an implementation of our work on using argumentation for evidence aggregation in the medical domain. It remains a work in progress, and is therefore likely to evolve over time. We have provided occasional snapshots in order to allow readers to replicate our work at a specific point in time, or to provide additional images, etc.

Interested reader are advised to look in the Docs section, where we post pre-press versions of papers, tutorials, slides, etc.

For those interested in the code we STRONGLY advise you NOT to check out from the repository. Much of our development happens off-line and we update the repo occasionally. If you are interested in contributing to the code, please contact us via email.